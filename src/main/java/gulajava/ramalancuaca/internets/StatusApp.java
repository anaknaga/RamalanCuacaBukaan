package gulajava.ramalancuaca.internets;

/**
 * Created by Gulajava Ministudio.
 */
public class StatusApp {

    //singleton untuk menampilkan notifikasi.
    //jika aplikasi sedang jalan, setel ke true, jika tidak, setel false
    //notifikasi aplikasi akan keluar ketika aplikasi sedang tidak berjalan

    private boolean statusAplikasiJalan = false;

    private static StatusApp ourInstance;

    public static StatusApp getInstance() {

        if (ourInstance == null) {

            ourInstance = new StatusApp();
        }

        return ourInstance;
    }

    private StatusApp() {
    }


    public boolean isStatusAplikasiJalan() {
        return statusAplikasiJalan;
    }

    public void setStatusAplikasiJalan(boolean statusAplikasiJalan) {
        this.statusAplikasiJalan = statusAplikasiJalan;
    }
}
