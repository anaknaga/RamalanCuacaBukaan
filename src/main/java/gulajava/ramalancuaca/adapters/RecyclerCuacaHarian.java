package gulajava.ramalancuaca.adapters;

import android.content.Context;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import gulajava.ramalancuaca.R;
import gulajava.ramalancuaca.database.RMCuacaHarian;
import gulajava.ramalancuaca.utilans.UtilanCuaca;
import io.realm.RealmResults;

/**
 * Created by Gulajava Ministudio.
 */
public class RecyclerCuacaHarian extends RecyclerView.Adapter<ViewHolder> {

    private Context mContext;
    private RealmResults<RMCuacaHarian> mCuacaHarianRealmResults;

    private final TypedValue typedvalues = new TypedValue();
    private int mBackground;
    private OnItemClickListener onItemClickListener;

    private static final int TIPE_VIEW_ISI_CUACAHARI = 13;
    private static final int TIPE_VIEW_ISI_KOSONG = 14;

    private int mIntPanjangData = 0;

    public RecyclerCuacaHarian(Context context, RealmResults<RMCuacaHarian> cuacaHarianRealmResults) {

        mContext = context;
        mCuacaHarianRealmResults = cuacaHarianRealmResults;

        mIntPanjangData = mCuacaHarianRealmResults.size();

        //setel agar recycler view ada latar belakangnya
        mContext.getTheme().resolveAttribute(android.R.attr.selectableItemBackground, typedvalues, true);
        mBackground = typedvalues.resourceId;

    }


    @Override
    public int getItemViewType(int position) {
        super.getItemViewType(position);

        int kodeView;

        if (mIntPanjangData > 0) {

            kodeView = TIPE_VIEW_ISI_CUACAHARI;

        } else {
            kodeView = TIPE_VIEW_ISI_KOSONG;
        }

        return kodeView;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view;
        if (viewType == TIPE_VIEW_ISI_CUACAHARI) {

            view = LayoutInflater.from(mContext).inflate(R.layout.desainbaris_cuaca_harian, parent, false);
            view.setBackgroundResource(mBackground);
            return new ViewHolderCuacaHarian(view);
        } else {

            view = LayoutInflater.from(mContext).inflate(R.layout.desainbaris_kondisikosong, parent, false);
            view.setBackgroundColor(ContextCompat.getColor(mContext, R.color.transparan));
            return new ViewHolderDataKosong(view);
        }
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        int tipeView = getItemViewType(position);

        switch (tipeView) {

            case TIPE_VIEW_ISI_CUACAHARI:

                //posisi indeks 0 dilewatkan karena sudah ditampilkan di tab halaman cuaca per jam.
                //yang ditampilkan hanya cuaca besok dst, dengan indeks > 0
                final int posisiKlik = position + 1;

                if (posisiKlik < mIntPanjangData) {

                    ViewHolderCuacaHarian viewHolderCuacaHarian = (ViewHolderCuacaHarian) holder;

                    RMCuacaHarian rmCuacaHarian = mCuacaHarianRealmResults.get(posisiKlik);

                    final int barisDb = rmCuacaHarian.getId_data();
                    long waktuMsCuaca = rmCuacaHarian.getLongwaktuMs();
                    String kodeNamaCuaca = rmCuacaHarian.getIdKodeCuaca();
                    String suhuMaks = rmCuacaHarian.getTemperaturMax();
                    String suhuMin = rmCuacaHarian.getTemperaturMin();


                    String hariCuaca = UtilanCuaca.konversiMsHari(waktuMsCuaca);
                    String namaCuaca = UtilanCuaca.getStatusCuacaTeks(kodeNamaCuaca);
                    int alamatGambar = UtilanCuaca.getGambarCuacaDariKodeKondisi(kodeNamaCuaca);
                    String suhuMaksFormat = UtilanCuaca.formatTemperature(mContext, suhuMaks);
                    String suhuMinFormat = UtilanCuaca.formatTemperature(mContext, suhuMin);

                    viewHolderCuacaHarian.mTextViewHariCuaca.setText(hariCuaca);
                    viewHolderCuacaHarian.mTextViewNamaCuaca.setText(namaCuaca);

                    Glide.with(mContext).load(alamatGambar)
                            .placeholder(R.drawable.weather_none_available)
                            .error(R.drawable.weather_none_available)
                            .into(viewHolderCuacaHarian.mImageViewKondisiCuaca);

                    viewHolderCuacaHarian.mTextViewSuhuMaks.setText(suhuMaksFormat);
                    viewHolderCuacaHarian.mTextViewSuhuMin.setText(suhuMinFormat);

                    viewHolderCuacaHarian.mViewContainer.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            if (onItemClickListener != null) {

                                Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {

                                        onItemClickListener.onItemClick(posisiKlik, barisDb);

                                    }
                                }, 250);
                            }
                        }
                    });
                }

                break;

            case TIPE_VIEW_ISI_KOSONG:
                //tidak ada apa apa
                break;
        }
    }

    @Override
    public int getItemCount() {

        //jika ada data lebih dari 0, balikkan nilai data
        //klo ga ada data, maka balikkan nilai 1, untuk tempat status kosong
        //panjang data berkurang satu, karena hanya diambil hari setelah hari ini
        if (mIntPanjangData > 0) {
            return mIntPanjangData - 1;
        } else {
            return 1;
        }
    }

    protected class ViewHolderCuacaHarian extends ViewHolder {

        private View mViewContainer;
        private TextView mTextViewHariCuaca;
        private TextView mTextViewNamaCuaca;
        private TextView mTextViewSuhuMaks;
        private TextView mTextViewSuhuMin;
        private ImageView mImageViewKondisiCuaca;

        public ViewHolderCuacaHarian(View itemView) {
            super(itemView);
            this.mViewContainer = itemView;

            mTextViewHariCuaca = (TextView) itemView.findViewById(R.id.teks_namahari);
            mTextViewNamaCuaca = (TextView) itemView.findViewById(R.id.teks_nama_kondisi);
            mTextViewSuhuMaks = (TextView) itemView.findViewById(R.id.teks_suhu_tinggi);
            mTextViewSuhuMin = (TextView) itemView.findViewById(R.id.teks_suhu_rendah);
            mImageViewKondisiCuaca = (ImageView) itemView.findViewById(R.id.gambar_kondisicuaca);
        }
    }


    //DATA KOSONG
    protected class ViewHolderDataKosong extends ViewHolder {

        public View mViewContainer;

        public ViewHolderDataKosong(View itemView) {
            super(itemView);
            mViewContainer = itemView;
        }
    }


    //INTERFACE JIKA LIST ITEM DI KLIK
    public interface OnItemClickListener {
        void onItemClick(int posisiKlik, int idDatabase);
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }


    //SEGARKAN DATA REALM
    public void segarkanDataRealm() {

        mIntPanjangData = mCuacaHarianRealmResults.size();
        RecyclerCuacaHarian.this.notifyItemRangeChanged(0, mIntPanjangData);

        RecyclerCuacaHarian.this.notifyDataSetChanged();
    }


}
