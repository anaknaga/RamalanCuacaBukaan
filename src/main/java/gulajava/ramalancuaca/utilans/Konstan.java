package gulajava.ramalancuaca.utilans;

import android.Manifest;

/**
 * Created by Gulajava Ministudio.
 */
public class Konstan {


    public static final String SERVER_CUACA = "http://api.openweathermap.org";

    public static final String PARAM_REQUEST_TIPEDATA = "mode";
    public static final String PARAM_REQUEST_JUMLAHHARI = "cnt";
    public static final String PARAM_REQUEST_LATITUDE = "lat";
    public static final String PARAM_REQUEST_LONGITUDE = "lon";
    public static final String PARAM_REQUEST_SATUAN_UNIT = "units";

    public static final String PARAM_ISI_JUMLAH_HARI = "14";
    public static final String PARAM_ISI_JUMLAH_JAM = "10";

    public static final int ID_BARIS_SATU_DB = 1;

    public static final String KODE_BROADCAST_SERVIS = "SERVIS_ACTIVITY";


    // waktu 1 hari dalam milidetik
    public static final long DAY_IN_MILLIS = 1000 * 60 * 60 * 24;

    // waktu 1 jam dalam milidetik
    public static final long HOUR1_IN_MILLIS = 1000 * 60 * 60;

    // permissi lokasi
    public static final String[] PERMISSIONS = {
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_NETWORK_STATE
    };

    public static final int ID_MINTALOKASI = 7;

    public static final String INTENT_BARISDB = "baris_databaseRealm";


}
